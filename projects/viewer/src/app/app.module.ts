import { DatePipe } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { VstLayoutComponent, VstLayoutModule } from 'projects/layout/src/public-api';
import { OtherComponentComponent } from './other-component/other-component.component';
import { AnotherComponent } from './another/another.component';
import { SomeComponent } from './some/some.component';
import { VSTRoutes } from './../../../../projects/layout/src/lib/vst-tabs/interfaces/vst-routes';
import { MiddlewareComponent } from './middleware/middleware.component';
import { VstGalleryUploadModule } from 'projects/gallery-upload/src/public-api';
import { VstCustomPanelModule, CustomPanelComponent } from 'projects/custom-panel/src/public-api';
import { ComponenteAComponent } from './components/componente-a/componente-a.component';
import { ComponenteBComponent } from './components/componente-b/componente-b.component';
import { ComponenteCComponent } from './components/componente-c/componente-c.component';
import { ComponenteDComponent } from './components/componente-d/componente-d.component';
import { ComponenteEComponent } from './components/componente-e/componente-e.component';
import { CustomNavbarWidgetComponent } from './custom-navbar-widget/custom-navbar-widget.component';


const routes2: VSTRoutes = [
    {
        path: "rtt", children: [
            { path: 'dashboard', component: CustomPanelComponent, openTab: true },
            { path: 'home', component: HomeComponent, openTab: true },
            { path: 'some-component', component: SomeComponent, openTab: true },
            { path: 'other-component', component: OtherComponentComponent, openTab: true },
            { path: 'another-component', component: AnotherComponent, openTab: true },
            { path: 'component-a', component: ComponenteAComponent, openTab: true },
            { path: 'component-b', component: ComponenteBComponent, openTab: true },
            { path: 'component-c', component: ComponenteCComponent, openTab: true },
            { path: 'component-d', component: ComponenteDComponent, openTab: true },
            { path: 'component-e', component: ComponenteEComponent, openTab: true },
        ]
    }
]

//// testanddo tabs
const routes: VSTRoutes = [
    {
        path: '', component: VstLayoutComponent, children: [
            {
                path: '', component: MiddlewareComponent, children: [
                    ...routes2
                ]
            }
        ]
    }
]

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        OtherComponentComponent,
        AnotherComponent,
        SomeComponent,
        MiddlewareComponent,
        ComponenteAComponent,
        ComponenteBComponent,
        ComponenteCComponent,
        ComponenteDComponent,
        ComponenteEComponent,
        CustomNavbarWidgetComponent
    ],
    imports: [
        BrowserModule,
        VstLayoutModule,
        VstGalleryUploadModule,
        RouterModule.forRoot(routes),
        VstCustomPanelModule
    ],
    providers: [
        DatePipe
    ],
    entryComponents: [
        CustomNavbarWidgetComponent
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
