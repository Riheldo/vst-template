import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'vst-other-component',
  templateUrl: './other-component.component.html',
  styleUrls: ['./other-component.component.scss']
})
export class OtherComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
