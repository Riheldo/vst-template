import { VstLayoutService } from './../../../layout/src/lib/vst-layout/services/vst-layout.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { CustomNavbarWidgetComponent } from './custom-navbar-widget/custom-navbar-widget.component';

@Component({
    selector: 'vst-root',
    template: '<router-outlet></router-outlet>',
    styles: []
})
export class AppComponent implements OnInit {

    constructor(private vstLayoutService: VstLayoutService) { }

    ngOnInit() {
        this.vstLayoutService.groups = [
            { title: "Home", pathUrl: "/rtt/home", iconClass: "fas fa-home" },
            {
                title: "Dashboard", pathUrl: "/rtt/dashboard", iconClass: "fas fa-tachometer-alt", items: [
                    { title: 'One', pathUrl: '/rtt/dashboard' }
                ]
            },
            {
                title: "Components", canClose: true, open: false, iconClass: "fas fa-archway", items: [
                    {
                        title: "Component 1", canClose: true, open: false, iconClass: "fas fa-address-book", items: [
                            {
                                title: "Component 2", canClose: true, open: false, iconClass: "fas fa-archway", items: [
                                    { title: "Component 3", pathUrl: "/rtt/other-component", iconClass: "fas fa-ad" },
                                ]
                            },
                        ]
                    },
                    { title: "Some Component", pathUrl: "/rtt/some-component", iconClass: "fas fa-address-card" }
                ]
            },
            { title: "Another Component", pathUrl: "/rtt/another-component", iconClass: "fas fa-ad" }
        ];
        this.vstLayoutService.logoSrc = "/assets/img/human-logo.png";
        this.vstLayoutService.menuMarkItem = true;
        this.vstLayoutService.menuAngleMarkPosition = 'right';
        this.vstLayoutService.tabsEnabled = true;
        // this.vstLayoutService.headerFixed = true;
        this.vstLayoutService.authWidgetEnable = true;
        this.vstLayoutService.authWidgetOptions = {
            title: "Algum Titulo",
            subTitle: "Igreja Batista Alagoinha",
            image: "assets/img/avatar.png",
            position: "navbar",
            imageElementId: 'imagem-id',
            onImageClick: () => {
                console.log('chamou aqui blz', document.getElementById('imagem-id'));
            },
            disableImageClick: false,
            options: [
                {
                    title: "Sair", iconClass: "far fa-bell", onClick: (option, e) => {
                        console.log('chamou click option: ', option);
                        console.log('chamou click e: ', e);
                    }
                },
                {
                    title: "Sair", iconClass: "fas fa-cog", onClick: (option, e) => {
                        console.log('chamou click option: ', option);
                        console.log('chamou click e: ', e);
                    }
                }
            ]
        }

        this.vstLayoutService.groups.push({ title: "Primeira parte", id: "pp1" });
        this.vstLayoutService.groups.push({ title: "Primeira parte 2", id: "pp2", items: [{ title: "Sub", id: "pp21", pathUrl: '/rtt/component-d' }] });
        this.vstLayoutService.pushOnGroup({ id: "pp1" }, [{ title: "Find Dinamico 1", pathUrl: '/rtt/component-a' }]);
        this.vstLayoutService.pushOnGroup({ id: "pp2" }, [{ title: "Find Dinamico 2", pathUrl: '/rtt/component-b' }]);
        this.vstLayoutService.pushOnGroup({ id: "pp21" }, [{ title: "Find Dinamico 21", pathUrl: '/rtt/component-c' }]);
        this.vstLayoutService.pushOnGroup({ id: "pp21" }, [{ title: "Find Dinamico 22", pathUrl: '/rtt/component-e' }]);

        this.vstLayoutService.tabsLimitExceeded.subscribe((e) => {
            console.log('LIMITE EXCEDIDO!');
        });

        this.vstLayoutService.useUnionTabs = true;
        this.vstLayoutService.useBodyTabs = true;
    }
}
