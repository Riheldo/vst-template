import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { VstGalleryUploadComponent } from 'projects/gallery-upload/src/public-api';
import { VstLayoutService } from 'projects/layout/src/public-api';

@Component({
    selector: 'vst-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    @ViewChild('galleryUpload', { static: false }) galleryUpload: VstGalleryUploadComponent;
    images: any[] = [];
    showCropper: boolean = false;
    croppedFile: any = '';
    imageChangedEvent: any = '';

    constructor(private datePipe: DatePipe, private vstLayoutService: VstLayoutService) {
        this.vstLayoutService.onMenuToggle.subscribe(v => {
            console.log("onMenuToggle: ", v);
        })
    }

    ngOnInit() { }

    prepareImageResponse = (e): string => {
        if (!e) return "";

        return e['link'];
    }

    onImageUploading(event) {
        this.imageChangedEvent = event;
        this.showCropper = true;
    }

    imageLoaded() {
        this.showCropper = true;
    }

    saveImage() {
        this.galleryUpload.processFile(this.croppedFile).then(v => {
            this.showCropper = false;
        });
    }
}