import { NgModule } from '@angular/core';
import { CustomPanelComponent } from './custom-panel/custom-panel.component';
import { CustomPanelService } from './services/custom-panel.service';
import { IncludePanelComponent } from './components/include-panel/include-panel.component';

@NgModule({
    declarations: [
        CustomPanelComponent,
        IncludePanelComponent
    ],
    providers: [
        CustomPanelService
    ],
    imports: [
    ],
    exports: [
        CustomPanelComponent
    ]
})
export class VstCustomPanelModule {
}
