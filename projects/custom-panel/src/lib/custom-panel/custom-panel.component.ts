import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'vst-custom-panel',
    templateUrl: './custom-panel.component.html',
    styleUrls: ['./custom-panel.component.scss']
})
export class CustomPanelComponent implements OnInit {

    constructor() { }

    ngOnInit() {
    }

}
