import { Route } from "@angular/router";

export interface VSTRoute extends Route {
    children?: VSTRoutes;
    openTab?: boolean;
    sharedBetweenTabs?: boolean;
    title?: string;
}

export declare type VSTRoutes = VSTRoute[];