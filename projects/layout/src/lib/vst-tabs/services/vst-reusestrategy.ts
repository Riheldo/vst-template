import { DetachedRouteHandle, RouteReuseStrategy, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute, Router } from '@angular/router';
import { VSTTabsService } from './vst-tabs.service';
import { VSTRoute } from '../interfaces/vst-routes';
import { OnDestroy } from '@angular/core';


function recoverStateUrlPath(activatedRoute: ActivatedRouteSnapshot): string {
    const routerState: RouterStateSnapshot = activatedRoute['_routerState'];
    if (routerState) {
        return routerState.url;
    }
    return "/";
}

// VERIFICA SE A ARVORE GERADA PELO NODE RAIZ TEM ALGUMA NODE QUE É TAB
function isNodeFromTab(activatedRoute: ActivatedRouteSnapshot): boolean {
    if ((activatedRoute.routeConfig as VSTRoute).openTab) return true;
    for (const childActivatedRoute of activatedRoute.children) {
        const result: boolean = isNodeFromTab(childActivatedRoute);
        if (result) return true;
    }
    return false;
}


// This impl. bases upon one that can be found in the router's test cases.
export class VSTCustomReuseStrategy implements RouteReuseStrategy {

    handlers: {[key: string]: DetachedRouteHandle} = {};

    constructor (private tabsService: VSTTabsService) {
        tabsService.registerCustomStrategy(this);
    }

    reset() {
        this.handlers = {};
    }

    manuallyDetach(keyPath: string): boolean {
        if (!this.handlers.hasOwnProperty(keyPath)) {
            return false;
        }
        try {
            if ((this.handlers[keyPath] as OnDestroy).ngOnDestroy) (this.handlers[keyPath] as OnDestroy).ngOnDestroy();
        } catch (error) {
            console.error("Error in ngOnDestroy when manuallyDetach component!");
            console.error(error);
        }
        this.handlers[keyPath] = null;
        return true;
    }

    // default: return false;
    /** Determines if this route (and its subtree) should be detached to be reused later */
    shouldDetach(route: ActivatedRouteSnapshot): boolean {
        return isNodeFromTab(route);
    }

    // default: {void}
    /**Stores the detached route.
     * Storing a `null` value should erase the previously stored value.
     */
    store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
        const urlPath: string = recoverStateUrlPath(route);
        this.handlers[urlPath] = handle;
    }

    // default: return false;
    /** Determines if this route (and its subtree) should be reattached */
    shouldAttach(route: ActivatedRouteSnapshot): boolean {
        const tabUrlPath: string = recoverStateUrlPath(route);
        if (route.routeConfig && (route.routeConfig as VSTRoute).openTab) {
            this.tabsService.add(tabUrlPath, (route.routeConfig as VSTRoute).title);
        }
        return !!this.retrieve(route);
    }

    // default: return null;
    /** Retrieves the previously stored route */
    retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        const urlPath: string = recoverStateUrlPath(route);
        return this.handlers[urlPath];
    }

    // default: same!
    /** Determines if a route should be reused */
    shouldReuseRoute(current: ActivatedRouteSnapshot, future: ActivatedRouteSnapshot): boolean {
        if (current.routeConfig && current.routeConfig.path) {
            // verificar se a rota faz parte de uma nova tab e se eh compartilhada
            if (isNodeFromTab(current) && !(current.routeConfig as VSTRoute).sharedBetweenTabs || isNodeFromTab(future) && !(future.routeConfig as VSTRoute).sharedBetweenTabs) {
                return false;
            }
        }
        return future.routeConfig === current.routeConfig;
    }

}
