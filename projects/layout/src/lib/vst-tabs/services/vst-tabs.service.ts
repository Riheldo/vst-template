import { Injectable, Injector, EventEmitter } from '@angular/core';
import { VSTCustomReuseStrategy } from './vst-reusestrategy';
import { Router, ActivatedRoute, RouterState, NavigationEnd } from '@angular/router';

export function actualUrlPath(activatedRoute: ActivatedRoute): string {
    return (activatedRoute['_routerState'] as RouterState).snapshot.url;
}

export class VSTTabModel {
    index: number = 0;
    count: number;
    title: string;
    url: string;
    selected: boolean;
    icon: string;
    data: any;

    constructor(private tabService: VSTTabsService) {}

    remove() {
        this.tabService.remove(this);
    }
    kill () {
        this.tabService.popTab(this);
    }
}

@Injectable({
    providedIn: "root"
})
export class VSTTabsService {

    public tabs: Array<VSTTabModel> = [];
    public tabsChanged: EventEmitter<Array<VSTTabModel>> = new EventEmitter();
    private count: number = 0;

    public customStrategy: VSTCustomReuseStrategy;
    private router: Router;
    private activatedRoute: ActivatedRoute;

    constructor(private injector: Injector) {
    }

    registerCustomStrategy(customStrategy: VSTCustomReuseStrategy) {
        this.customStrategy = customStrategy;
    }

    checkInitParams() {
        if (!this.activatedRoute) {
            this.activatedRoute = this.injector.get(ActivatedRoute);
        }
        if (!this.router) {
            this.router = this.injector.get(Router);
            this.router.events.subscribe((routerEvent) => {
                if (routerEvent instanceof NavigationEnd) {
                    this.activating(actualUrlPath(this.activatedRoute));
                }
            });
        }
    }

    add(urlPath: string, title?: string) {
        this.checkInitParams();
        // verificar se jah tem caminho adicionado
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].url === urlPath) {
                return;
            }
        }
        const tabModel: VSTTabModel = new VSTTabModel(this);
        tabModel.title = (title)? title:urlPath;
        tabModel.selected = true;
        tabModel.count = this.count++;
        tabModel.url = urlPath;
        this.tabs.push(tabModel);
        this.tabsChanged.emit(this.tabs);
        this.calculatePosition();
    }

    retriveSideTab(tabModel: VSTTabModel): VSTTabModel {
        const currPosition = this.tabs.indexOf(tabModel);
        return this.tabs[(currPosition === 0) ? 1 : currPosition - 1];
    }

    remove(tabModel: VSTTabModel) {
        if (this.tabs.length === 1) return;
        // verificar se esta fechando a tab da view corrente
        if (window.location.pathname.indexOf(tabModel.url) !== -1) {
            const navigateToTabModel = this.retriveSideTab(tabModel);
            this.router.navigateByUrl(navigateToTabModel.url);
            setTimeout(() => {
                // this.popTab(tabModel);
                // this.calculatePosition();
                // this.customStrategy.manuallyDetach(tabModel.url);
                this.kill(tabModel);
            }, 80);
        } else {
            // this.popTab(tabModel);
            // this.calculatePosition();
            // this.customStrategy.manuallyDetach(tabModel.url);
            this.kill(tabModel);
        }
    }

    kill(tabModel: VSTTabModel) {
        this.popTab(tabModel);
        this.calculatePosition();
        this.customStrategy.manuallyDetach(tabModel.url);
    }

    disposeAll() {
        this.tabs.forEach((tab) => {
            this.customStrategy.manuallyDetach(tab.url);
        });
        this.customStrategy.reset();
        this.tabs = [];
        this.tabsChanged.emit(this.tabs);
    }

    activatedTab(): VSTTabModel {
        const currUrlPath = actualUrlPath(this.activatedRoute);
        for (const auxTab of this.tabs) {
            if (currUrlPath == auxTab.url) return auxTab;
        }
        return null;
    }

    activating(keyPath: string) {
        for (let i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].url === keyPath) {
                this.tabs[i].selected = true;
            } else {
                this.tabs[i].selected = false;
            }
        }
    }

    calculatePosition(): void {
        for (let i = 0; i < this.tabs.length; i++) {
            this.tabs[i].index = i;
        }
    }

    popTab(tabModel: VSTTabModel) {
        this.tabs.splice(this.tabs.indexOf(tabModel), 1);
        this.tabsChanged.emit(this.tabs);
    }
}
