//
// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteReuseStrategy, RouterModule } from '@angular/router';

//
// VENDORS
import { DragulaModule, DragulaService } from 'ng2-dragula';

//
// APP
import { VSTCustomReuseStrategy } from './services/vst-reusestrategy';
import { VSTTabsService } from './services/vst-tabs.service';
import { VSTTabsComponent } from './components/vst-tabs/vst-tabs.component';
import { VsUnionTabsComponent } from './components/vs-union-tabs/vs-union-tabs.component';
import { VsTabsContainerComponent } from './components/tabs-container.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        DragulaModule,
    ],
    providers: [
        {provide: RouteReuseStrategy, useClass: VSTCustomReuseStrategy, deps: [VSTTabsService]},
        DragulaService,
    ],
    declarations: [
        VSTTabsComponent,
        VsUnionTabsComponent,
        VsTabsContainerComponent,
    ],
    entryComponents: [
        VSTTabsComponent,
        VsUnionTabsComponent,
    ],
    exports: [
        VSTTabsComponent,
        VsUnionTabsComponent,
        VsTabsContainerComponent,
    ]
})
export class VSTTabsModule { }
