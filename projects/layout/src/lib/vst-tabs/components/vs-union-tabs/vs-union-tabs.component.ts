import { Component, OnInit, OnDestroy } from '@angular/core';
import { VSTTabsService, VSTTabModel } from '../../services/vst-tabs.service';
import { Router } from '@angular/router';

@Component({
    selector: 'vs-union-tabs',
    templateUrl: './vs-union-tabs.component.html',
    styleUrls: ['./vs-union-tabs.component.scss']
})
export class VsUnionTabsComponent implements OnInit, OnDestroy {

    maxTabsWidth: number;
    sumTabsWidth: number;
    tabWidth: number = 100;
    moviment: number = 0;
    taxMove: number = 0;
    maxMove: number;
    tabOverlapGap: number = 3;
    isMac: boolean = false;
    isWin: boolean = false;

    constructor(public tabsService: VSTTabsService, private router: Router) {}

    resizeEvent = (e) => {
        this.refreshWidth();
    }

    ngOnInit() {
        this.refreshWidth();
        window.addEventListener("resize", this.resizeEvent);
        this.tabsService.tabsChanged.subscribe(() => {
            this.refreshWidth();
        });
    }

    ngOnDestroy() {
        window.removeEventListener("resize", this.resizeEvent);
    }

    refreshWidth() {
        this.isMac = navigator.platform.toUpperCase().indexOf('MAC')>=0;
        this.isWin = navigator.platform.toUpperCase().indexOf('WIN')>=0;

        // calculate max tab width
        const ele = document.getElementsByClassName('vs-union-tabs')[0];
        this.maxTabsWidth = ele.clientWidth;
        let tabSize: number = this.maxTabsWidth / this.tabsService.tabs.length;
        if (tabSize<124) tabSize = 124;
        this.tabWidth = tabSize;
        this.sumTabsWidth = tabSize * this.tabsService.tabs.length;

        // calculate max moviment
        this.maxMove = Math.abs(this.maxTabsWidth - this.sumTabsWidth);
    }

    whellScroll(e: WheelEvent) {
        e.preventDefault();
        const moviment: number = e.deltaX || e.deltaY;
        // console.log(`whellScroll => x: ${e.deltaX} y: ${e.deltaY}`);
        if (moviment > 0) {
            if (this.moviment + moviment >= this.maxMove) this.moviment = this.maxMove;
            else this.moviment += moviment;
        } else {
            // movimento negativo, redundancia para nao precisar apagar se algo der eerrado
            if (this.moviment + moviment <= 0) this.moviment = 0;
            else this.moviment += moviment;
        }
        if (this.moviment == 0) this.taxMove = 0;
        else this.taxMove = this.moviment / this.maxMove;
    }

    calculateLeftMargin(idx: number) {
        let startTab = idx * this.tabWidth;
        let toReturn;
        if (idx == 0) return startTab;
        if (idx === this.tabsService.tabs.length-1) return startTab - this.maxMove;
        
        let freeSpace = (this.tabWidth - this.tabOverlapGap) * idx;
        if (freeSpace < this.moviment) {
            if (freeSpace <= this.moviment) {
                toReturn = (idx * this.tabOverlapGap);
            } else {
                toReturn = startTab - (this.moviment + freeSpace);
            }
        } else {
            toReturn = startTab - this.moviment;
            const recoil = this.maxTabsWidth - this.tabWidth - this.tabOverlapGap * (this.tabsService.tabs.length-idx);
            if (toReturn > recoil) {
                toReturn = recoil;
            }
        }

        return toReturn;
    }

    clickOnTab(tabItem: VSTTabModel) {
        this.router.navigateByUrl(tabItem.url);
    }

    closeTab(e: Event, tabItem: VSTTabModel) {
        e.preventDefault();
        e.stopPropagation();
        tabItem.remove();
    }

}
