import { Component, Input, SimpleChanges, OnChanges, ComponentFactoryResolver, Injector, ViewContainerRef } from '@angular/core';
import { VsUnionTabsComponent } from './vs-union-tabs/vs-union-tabs.component';
import { VSTTabsComponent } from './vst-tabs/vst-tabs.component';

@Component({
    selector: 'vst-tabs-container',
    template: '',
})
export class VsTabsContainerComponent implements OnChanges {
    @Input() unionTabs: boolean;

    constructor(private viewRef: ViewContainerRef, private factoryResolver: ComponentFactoryResolver) {}

    ngOnChanges(changes: SimpleChanges) {
        if (changes.unionTabs && changes.unionTabs.currentValue !== changes.unionTabs.previousValue && !changes.unionTabs.firstChange) {
            this.renderComponent();
        }
    }

    ngOnInit() {
        this.renderComponent();
    }

    renderComponent() {
        let componentClass;
        if (this.unionTabs) {
            componentClass = VsUnionTabsComponent;
        } else {
            componentClass = VSTTabsComponent;
        }
        this.viewRef.remove(0);
        const componentFac = this.factoryResolver.resolveComponentFactory(componentClass);
        this.viewRef.createComponent(componentFac);
    }
}