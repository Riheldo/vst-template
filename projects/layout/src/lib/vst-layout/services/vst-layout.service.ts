import { Injectable, EventEmitter } from '@angular/core';
import { VstItemMenu } from '../interfaces/menu.interfaces';
import { VstAuthWidgetOptionsI } from '../components/vst-auth-widget/vst-auth-widget-options.interface';
import { VstMenuAnglePosition } from '../model/menu-angle-position.model';

export interface VstItemMenuParamI {
    id?: string;
    title?: string;
    iconSrc?: string;
    iconClass?: string;
    pathUrl?: string;
}

function getGroup(groupParams: VstItemMenuParamI, groups: Array<VstItemMenu> | VstItemMenu): VstItemMenu {
    if (groups instanceof Array) {
        for (const groupItem of groups) {
            const result = getGroup(groupParams, groupItem);
            if (result) return result;
        }
    } else {
        const open = Object.keys(groupParams).filter((keyParam) => {
            const valueParam = groupParams[keyParam];
            if (!valueParam) return keyParam;
            if (groups[keyParam] == valueParam) return;
            return keyParam;
        });
        if (open.length === 0) return groups;
        else if (groups.items && groups.items.length) return getGroup(groupParams, groups.items);
    }
}

@Injectable({ providedIn: "root" })
export class VstLayoutService {
    filterEnabled: boolean = true;
    headerFixed: boolean = false;
    tabsEnabled: boolean = true;
    breadcumbsEnabled: boolean = true;
    authWidgetEnable: boolean = false;
    menuMarkItem: boolean = false;
    menuAngleMarkPosition: VstMenuAnglePosition = 'left'; // define a posição do ícone arrow dos menus com ítens (left ou right)
    groups: Array<VstItemMenu> = [];
    logoSrc: string = "";
    authWidgetOptions: VstAuthWidgetOptionsI = {};
    onMenuToggle: EventEmitter<boolean> = new EventEmitter<boolean>();
    tabsLimit: number = 5;
    tabsLimitExceeded: EventEmitter<any> = new EventEmitter<any>();

    /**
     * Argumento informa ao template se é pra renderizar as tabs junto a barra de navegação, ou junto ao corpo
     * se ativado as configurações das tabs da barra de navegação deixarão de funcionar, e as tabs no corpo serão 
     * renderizadas.
     */
    useBodyTabs: boolean = false;
    /**
     * Existe um novo componente de tabs, se chama tabs unidas, para usa-las basta ativar o parametro abaixo,
     * caso contrário a antiga estrutura de tabs será usada.
     */
    useUnionTabs: boolean = false;

    pushOnGroup(filter: VstItemMenuParamI, groups: Array<VstItemMenu>, options: { mergeIfnf: boolean } = { mergeIfnf: true }) {
        const group = getGroup(filter, this.groups);
        if (group) {
            if (group.items && group.items.length) group.items.push(...groups);
            else group.items = groups;
        } else {
            this.groups.push(...groups);
        }
    }
}