// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations"
import { HttpClientModule } from '@angular/common/http';

// COMPONENTS
import { VstLayoutComponent } from './vst-layout.component';
import { VstFilterMenuComponent } from './components/vst-menu/vst-filter-menu.component';
import { VstMenuTreeComponent } from './components/vst-menu/vst-menutree.component';
import { VstBreadcrumbsComponent } from './components/vst-breadcrumbs/vst-breadcrumbs.component';
import { VstAuthWidgetComponent } from './components/vst-auth-widget/vst-auth-widget.component';
import { VSTTabsModule } from '../vst-tabs/vst-tabs.module';
import { SideMenuComponent } from './components/side-menu/side-menu.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        VSTTabsModule,
    ],
    declarations: [
        VstLayoutComponent,
        VstFilterMenuComponent,
        VstMenuTreeComponent,
        VstBreadcrumbsComponent,
        VstAuthWidgetComponent,
        SideMenuComponent,
    ],
    exports: [
        VstLayoutComponent,
        VstFilterMenuComponent,
        VstMenuTreeComponent,
        VstBreadcrumbsComponent,
    ]
})
export class VstLayoutModule { }
