import { Component, OnInit } from '@angular/core';
import { VstLayoutService } from './services/vst-layout.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'vst-layout',
    templateUrl: './vst-layout.component.html',
    styleUrls: ['./vst-layout.component.scss']
})
export class VstLayoutComponent implements OnInit {

    // ATIVA MENU GAVETA
    gaveta: boolean = false;
    // CONTROLA ABERTURA NA RESPONSIVIDADE E EM EVENTOS COM MOUSE IN E OUT
    respGaveta: boolean = false;
    // ATIVADO QUANDO MOUSE ENTRA NAS DIMENSÕES DO MENU
    mouseInMenu: boolean = false;
    // ATIVADO QUANDO EM TELA RESPONSIVA
    responsive: boolean = false;

    constructor(public layoutService: VstLayoutService, router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                if (this.respGaveta) {
                    this.respGaveta = false;
                }
            }
        });

        const auxResize: any = window.onresize;
        window.onresize = (ev) => {
            this.checkResponsive();
            if (typeof auxResize === "function") auxResize(ev);
        };
        this.checkResponsive();
    }

    checkResponsive() {
        if (window.innerWidth < 1013) this.responsive = true;
        else this.responsive = false;
    }

    ngOnInit() { }

    toggleMenu() {
        this.gaveta = !this.gaveta;
        this.layoutService.onMenuToggle.emit(this.gaveta);
    }

    toggleShow() {
        this.respGaveta = !this.respGaveta;
        this.layoutService.onMenuToggle.emit(this.respGaveta);
    }

    mouseIn() {
        this.mouseInMenu = true;
    }

    mouseOut() {
        this.mouseInMenu = false;
    }
}