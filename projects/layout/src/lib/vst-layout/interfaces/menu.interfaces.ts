export interface VstItemMenu {
    id?: string;
    class?: string | Array<string>;
    title: string;
    iconSrc?: string;
    iconSrcActive?: string;
    iconClass?: string;
    iconClassActive?: string;
    pathUrl?: string;
    open?: boolean;
    canClose?: boolean; // define se o menu pode ou não ser fechado
    items?: Array<VstItemMenu>;
    disable?: boolean; 
    isGroup?: boolean;
    
    onClick?: (item?: VstItemMenu, event?: any) => any;
}