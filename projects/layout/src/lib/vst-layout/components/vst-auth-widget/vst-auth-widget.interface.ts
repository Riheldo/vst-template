export interface VstAuthWidgetI {
    id?: string;
    class?: string | Array<string>;
    title: string;
    iconSrc?: string;
    iconClass?: string;
    pathUrl?: string;
    
    onClick?: (item?: VstAuthWidgetI, event?: any) => any;
}
