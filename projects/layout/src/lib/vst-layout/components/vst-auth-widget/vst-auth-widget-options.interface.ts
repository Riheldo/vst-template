import { VstAuthWidgetPosition } from './vst-auth-widget-position.model';
import { VstAuthWidgetI } from './vst-auth-widget.interface';
import { Type, EventEmitter } from '@angular/core';

export interface VstAuthWidgetOptionsI {
    title?: string;
    subTitle?: string;
    image?: string;
    imageElementId?: string;
    onImageClick?: Function;
    disableImageClick?: boolean;
    options?: Array<VstAuthWidgetI>;
    position?: VstAuthWidgetPosition;
    navbarCustomComponentClass?: Type<any>;
}