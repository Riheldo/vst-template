import { Component, Input, Type, ViewChild, ViewContainerRef, OnInit, Injector, ComponentFactoryResolver, OnChanges } from '@angular/core';
import { VstAuthWidgetI } from './vst-auth-widget.interface';
import { Router } from '@angular/router';
import { VstLayoutService } from '../../services/vst-layout.service';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
    selector: 'vst-auth-widget',
    templateUrl: './vst-auth-widget.component.html',
    styleUrls: ['./vst-auth-widget.component.scss'],
    animations: [
        trigger('toggle-closeOptions', [
            state('false', style({ height: '*', overflow: 'hidden' })),
            state('true', style({ height: '0', overflow: 'hidden' })),
            transition('1 => 0', animate('200ms ease-in')),
            transition('0 => 1', animate('200ms ease-out'))
        ])
    ]
})
export class VstAuthWidgetComponent implements OnInit, OnChanges {

    @ViewChild('vc', { read: ViewContainerRef, static: false }) private vc: ViewContainerRef;
    @Input() closeOptions: boolean;
    @Input() customComponentClass: Type<any>;

    constructor(private router: Router, public layoutService: VstLayoutService, private injector: Injector, private compFactoryResolver: ComponentFactoryResolver) { }

    ngOnInit() { }

    ngOnChanges(changes) {
        if (changes.customComponentClass) {
            if (this.layoutService.authWidgetOptions.navbarCustomComponentClass) {
                const componentRef = this.compFactoryResolver.resolveComponentFactory(this.layoutService.authWidgetOptions.navbarCustomComponentClass).create(this.injector);

                setTimeout(() => {
                    this.vc.insert(componentRef.hostView);
                });
            }
        }
    }

    optionClicked(option: VstAuthWidgetI, e: any) {
        if (option.pathUrl) return this.router.navigateByUrl(option.pathUrl);
        if (typeof option.onClick === "function") return option.onClick(option, e);
    }

    onAuthImgClick() {
        if (!this.layoutService.authWidgetOptions.disableImageClick)
            this.layoutService.authWidgetOptions.onImageClick();
    }
}