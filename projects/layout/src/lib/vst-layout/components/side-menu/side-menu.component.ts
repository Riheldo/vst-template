import { Component, Input, Output, EventEmitter } from '@angular/core';
import { VstLayoutService } from '../../services/vst-layout.service';

@Component({
  selector: 'vst-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent {

    @Input() mouseInMenu: boolean;
    @Input() responsive: boolean;
    @Input() gaveta: boolean;
    @Output() gavetaChange: EventEmitter<boolean> = new EventEmitter();
    @Output() respGavetaChange: EventEmitter<boolean> = new EventEmitter();
    // ATIVA O FILTRO
    filterActivated: boolean = false;

    constructor(public layoutService: VstLayoutService) { }

}
