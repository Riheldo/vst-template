import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, PRIMARY_OUTLET, Params } from '@angular/router';
import { VstLayoutService } from '../../services/vst-layout.service';

export interface VstBreadcrumbInterface {
    title: string;
    params?: Params;
    urlPath: string;
}

@Component({
    selector: 'vst-breadcrumbs',
    templateUrl: './vst-breadcrumbs.component.html',
    styleUrls: ['./vst-breadcrumbs.component.scss']
})
export class VstBreadcrumbsComponent implements OnInit {

    pages: Array<VstBreadcrumbInterface> = [
        { title: "Início", urlPath: "/" },
        { title: "Modulo Viagem", urlPath: "/modviagem" },
        { title: "Quartos", urlPath: "/modviagem/rooms" },
        { title: "Quarto #905", urlPath: "/modviagem/room/95" },
    ];

    constructor(private router: Router, private route: ActivatedRoute, public layout: VstLayoutService) { }

    ngOnInit() {
        this.router.events.subscribe((routerEvent) => {
            if (routerEvent instanceof NavigationEnd) {
                this.pages = this.getBreadcrumbs(this.route.root);
            }
        });
        this.pages = this.getBreadcrumbs(this.route.root);
    }

    getBreadcrumbs(route: ActivatedRoute, url: string="", breadcrumbs: Array<VstBreadcrumbInterface>=[]): Array<VstBreadcrumbInterface> {
        //get the child routes
        let children: ActivatedRoute[] = route.children;

        //return if there are no more children
        if (children.length === 0) {
            return breadcrumbs;
        }

        //iterate over each children
        for (let child of children) {
            //verify primary route
            if (child.outlet !== PRIMARY_OUTLET) {
                continue;
            }

            //verify the custom data property "breadcrumb" is specified on the route
            // if (!(child.snapshot.routeConfig as VstRoute).title) {
            //     return this.getBreadcrumbs(child, url, breadcrumbs);
            // }

            //get the route's URL segment
            let routeURL: string = child.snapshot.url.map(segment => segment.path).join("/");

            //append route URL to URL
            url += `/${routeURL}`;
            if (url.indexOf("//") == 0) url = url.substr(1);

            //add breadcrumb
            let title: string = (child.snapshot.routeConfig as any).title;
            if (!title) {
                title = (url=="/") ? "Início" : url;
            }

            let breadcrumb: VstBreadcrumbInterface = {
                title: title,
                params: child.snapshot.params,
                urlPath: url
            };
            breadcrumbs.push(breadcrumb);

            //recursive
            return this.getBreadcrumbs(child, url, breadcrumbs);
        }
    }

}
