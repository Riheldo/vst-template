import { Component, OnInit, Input, SimpleChanges, OnChanges, EventEmitter, Output } from '@angular/core';
import { VstItemMenu } from '../../interfaces/menu.interfaces';
import { ActivatedRoute, RouterState, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { VstLayoutService } from '../../services/vst-layout.service';
import { VSTTabsService } from '../../../vst-tabs/services/vst-tabs.service';

function actualUrlPath(activatedRoute: ActivatedRoute): string {
    return (activatedRoute['_routerState'] as RouterState).snapshot.url;
}

@Component({
    selector: 'vst-menutree',
    templateUrl: './vst-menutree.component.html',
    styleUrls: ['./vst-menutree.component.scss'],
    animations: [
        trigger('openChanged', [
            state('true', style({ height: '*', overflow: 'hidden' })),
            state('false', style({ height: '0', overflow: 'hidden' })),
            transition('1 => 0', animate('200ms ease-in')),
            transition('0 => 1', animate('200ms ease-out'))
        ])
    ]
})
export class VstMenuTreeComponent implements OnInit {

    @Input() itemsMenu: Array<VstItemMenu>;
    @Input() markItemActive: boolean = false;
    @Input() forceCloseGroup: boolean = false;
    @Input() isChildItem: boolean = false;
    activatedItem: VstItemMenu = undefined;
    itemMarkedEventSub: Subscription;

    constructor(private router: Router, private route: ActivatedRoute, public layoutService: VstLayoutService,
        private vstTabsService: VSTTabsService) { }

    ngOnInit() {
        this.itemsMenu.forEach(itemMenu => {
            itemMenu.canClose = (typeof itemMenu.canClose === 'undefined') ? true : itemMenu.canClose;
            itemMenu.class = (typeof itemMenu.class === 'undefined') ? '' : itemMenu.class;
            itemMenu.id = (typeof itemMenu.id === 'undefined') ? '' : itemMenu.id;
            itemMenu.open = (typeof itemMenu.open === 'undefined') ? true : itemMenu.open;
        });

        this.router.events.subscribe((routerEvent) => {
            if (routerEvent instanceof NavigationEnd) {
                this.activatedItem = null;
                this.defineActivatedAnchor();
            }
        });
    }

    itemMenuClicked(event: Event, itemMenu: VstItemMenu) {
        event.stopPropagation();
        let result = !!itemMenu.pathUrl;

        if (itemMenu && typeof itemMenu.onClick === "function") {
            result = itemMenu.onClick(itemMenu, event);
        }

        if (result !== false) {
            if (this.vstTabsService.tabs.length < this.layoutService.tabsLimit || this.layoutService.useUnionTabs) {
                this.router.navigateByUrl(itemMenu.pathUrl).catch((reason) => {
                    console.error("Não foi possível a navegação. ", reason);
                });
            } else this.layoutService.tabsLimitExceeded.emit();
        }
    }

    toggleTree(itemMenu: VstItemMenu) {
        if (!itemMenu.canClose) return;
        itemMenu.open = !itemMenu.open;
    }

    defineActivatedAnchor() {
        const actualUrl = actualUrlPath(this.route);
        for (let item of this.itemsMenu) {
            if (item && item.pathUrl == actualUrl) {
                this.activatedItem = item;
                break;
            }
        }
    }

}