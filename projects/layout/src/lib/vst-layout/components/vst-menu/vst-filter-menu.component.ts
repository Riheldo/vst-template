import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VstItemMenu } from '../../interfaces/menu.interfaces';
import { VstLayoutService } from '../../services/vst-layout.service';


@Component({
    selector: 'vst-filter-menu',
    templateUrl: './vst-filter-menu.component.html',
    styleUrls: ['./vst-filter-menu.component.scss']
})
export class VstFilterMenuComponent {

    @Input() menuItems: Array<VstItemMenu> = [];

    @Input() filtering: boolean = false;
    @Output() filteringChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    filterGroup: VstItemMenu = { canClose: false, title: "Resultado", items: [] };

    public _inputSearch: string = '';
    public get inputSearch(): string {
        return this._inputSearch;
    }
    public set inputSearch(val: string) {
        this._inputSearch = val;
        this.filterGroup.items = [];
        if (val.length == 0) {
            this.filteringChange.emit(false);
        } else {
            this.filteringChange.emit(true);
            this.filter();
        }
    }

    constructor(public layout: VstLayoutService) { }

    resetMenuSearch() {
        this.inputSearch = '';
    }

    filter() {
        const filterkey: string = this.inputSearch.toLowerCase();

        let compareItem = (item: VstItemMenu) => {
            if (item.items && item.items.length) return item.items.forEach(compareItem);
            if (item.title.toLowerCase().indexOf(filterkey) >= 0) {
                this.filterGroup.items.push(item);
            }
        }
        this.menuItems.forEach(compareItem);
    }

}
