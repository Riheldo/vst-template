import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VstHeaderComponent } from './vst-header.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [VstHeaderComponent],
  exports: [VstHeaderComponent]
})
export class VstHeaderModule { }
