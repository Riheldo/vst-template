// modulo do layout
export * from './lib/vst-layout/vst-layout.module';
export * from './lib/vst-layout/vst-layout.component';
export * from './lib/vst-layout/services/vst-layout.service';

// tabs
export * from './lib/vst-tabs/services/vst-tabs.service';
export * from './lib/vst-tabs/interfaces/vst-routes';

// HEADER
export * from './lib/vst-header/vst-header.module';
export * from './lib/vst-header/vst-header.component';