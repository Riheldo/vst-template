import { HttpClientModule } from '@angular/common/http';
import { DragulaModule } from 'ng2-dragula';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { VstGalleryUploadComponent } from './vst-gallery-upload.component';

@NgModule({
  declarations: [VstGalleryUploadComponent],
  imports: [
    CommonModule,
    DragulaModule,
    HttpClientModule
  ],
  exports: [VstGalleryUploadComponent]
})
export class VstGalleryUploadModule { }