import { HttpClient } from '@angular/common/http';
import { DragulaService } from 'ng2-dragula';
import { Component, OnInit, EventEmitter, Output, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'vst-gallery-upload',
  templateUrl: './vst-gallery-upload.component.html',
  styleUrls: ['./vst-gallery-upload.component.scss']
})
export class VstGalleryUploadComponent implements OnInit {

  readonly gId: string = String(Math.random()).substr(2);
  readonly drakeName: string = 'GALLERYDRAGULADND' + this.gId;
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;
  @Output() valueChange: EventEmitter<Array<string>> = new EventEmitter();
  @Input() value: Array<string> = [];
  @Input() limit: number = 99999;
  @Input() col: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 = 3;
  @Input() uploadUrl: string;
  @Input() responseDataKey: string | ((e: any) => string);
  @Input() roundImage: boolean = false;
  @Input() cancelProcessFile: boolean = false;
  @Output() onUploading: EventEmitter<any> = new EventEmitter<any>();
  @Input() inputText: string = 'Selecione a foto';
  @Input() btnRemoveText: string = 'Remover';
  @Input() cancelDragDrop: boolean = false;
  @Input() fileUploadName: string = 'uploadFile';

  constructor(private http: HttpClient, private dragulaService: DragulaService) { }

  ngOnInit() {
    if (!this.value || !(this.value instanceof Array)) {
      this.value = [];
      this.valueChanged();
    }

    if (this.cancelDragDrop) {
      this.dragulaService.cancel(this.drakeName);
    }
  }

  valueChanged() {
    this.valueChange.emit(this.value);
  }

  removeImage(i) {
    this.value.splice(i, 1);
    this.valueChanged();
  }

  successUploaded(e) {
    if (!this.value || !(this.value instanceof Array)) this.value = [];

    if (typeof this.responseDataKey === 'function') this.value.push(this.responseDataKey(e));
    else this.value.push(e[this.responseDataKey]);

    this.valueChanged();
  }

  upload(event: any): void {
    this.onUploading.emit(event);

    if (!this.cancelProcessFile) {
      const fileList: FileList = event.target.files;

      if (fileList.length > 0) {
        const selectedFile = fileList[0];

        this.processFile(selectedFile);
      }
    }
  }

  processFile(selectedFile: any): Promise<any> {
    return new Promise((resolve, reject) => {
      const uploadData = new FormData();
      uploadData.append(this.fileUploadName, selectedFile, selectedFile.name);

      this.http.post(this.uploadUrl, uploadData).subscribe(res => {
        let resObject = JSON.parse(res['_body']);

        this.successUploaded(resObject);

        resolve(resObject);
      });
    });
  }
}