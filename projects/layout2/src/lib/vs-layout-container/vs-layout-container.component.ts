import { Component, OnInit, ContentChildren, AfterViewInit } from '@angular/core';
import { VsNavContainerComponent } from '../vs-nav-container/vs-nav-container.api';

@Component({
    selector: 'vs-layout-container',
    templateUrl: './vs-layout-container.component.html',
    styleUrls: ['./vs-layout-container.component.scss']
})
export class VsLayoutContainerComponent implements OnInit, AfterViewInit {

    @ContentChildren(VsNavContainerComponent) navContainerComponent;

    constructor() { }

    ngOnInit() {
        console.log("navContainerComponent: ", this.navContainerComponent);
    }

    ngAfterViewInit() {
        console.log("navContainerComponent: ", this.navContainerComponent);
    }

}
