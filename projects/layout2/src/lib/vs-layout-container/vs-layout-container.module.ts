import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VsLayoutContainerComponent } from './vs-layout-container.component';
import { VsNavContainerModule } from '../vs-nav-container/vs-nav-container.api';
import { VsMenuContainerModule } from '../vs-menu-container/vs-menu-container.api';

@NgModule({
    imports: [
        CommonModule,
        VsNavContainerModule,
        VsMenuContainerModule,
    ],
    declarations: [VsLayoutContainerComponent],
    exports: [VsLayoutContainerComponent],
})
export class VsLayoutContainerModule { }
