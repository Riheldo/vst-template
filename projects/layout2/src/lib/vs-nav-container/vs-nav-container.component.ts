import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'vs-nav-container',
    templateUrl: './vs-nav-container.component.html',
    styleUrls: ['./vs-nav-container.component.scss']
})
export class VsNavContainerComponent {

    @Input() height: string = "60px";
    @Input() left: string = "0";
    @Input() right: string = "0";

}
