import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VsNavContainerComponent } from './vs-nav-container.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        VsNavContainerComponent,
    ],
    exports: [VsNavContainerComponent],
})
export class VsNavContainerModule { }
