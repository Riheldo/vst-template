import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VsMenuContainerComponent } from './vs-menu-container.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [VsMenuContainerComponent],
  exports: [VsMenuContainerComponent],
})
export class VsMenuContainerModule { }
