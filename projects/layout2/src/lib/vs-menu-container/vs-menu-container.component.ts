import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
    selector: 'vs-menu-container',
    templateUrl: './vs-menu-container.component.html',
    styleUrls: ['./vs-menu-container.component.scss']
})
export class VsMenuContainerComponent {

    @Input() width: string = '250px';
    @Input() top: string = '40px';

    constructor() { }

}
