//////
////// ANGULAR
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//////
////// VENDORS*
import { VsMenuContainerModule, VsLayoutContainerModule, VsNavContainerModule } from '../../../layout2/src/public-api';

//////
////// INTERNS
import { AppComponent } from './app.component';


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        VsMenuContainerModule,
        VsLayoutContainerModule,
        VsNavContainerModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
